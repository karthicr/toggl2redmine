#!/usr/bin/env python

import argparse
import logging
import yaml
import sys

from datetime import date, datetime

from redmine import Redmine
from toggl import Toggl
from utils import get_class


# setup handler for unhandled exceptions
def handle_uncaught_exceptions(exc_type, exc_value, exc_traceback):
    """Handles uncaught exception. Uses logging configuration to log errors."""
    # ignore keyboard interrupt so program can be killed by calling Ctrl + C from terminal
    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return
    # use the python logging module to log exception
    logger.critical('Uncaught exception', exc_info=(exc_type, exc_value, exc_traceback))


def date_from_iso_format(value=None):
    """Converts a given ISO 8601 date string to python date instance."""
    try:
        return datetime.strptime(value, '%Y-%m-%d').date()
    except ValueError:
        raise argparse.ArgumentTypeError('Invalid date format: {}. Should be of ISO 8601 date format: YYYY-MM-DD.'.format(value))


def main(config_file, since, until, transformer_class):
    # parse the config first
    try:
        with open('config.yml', 'r') as stream:
            config = yaml.load(stream)
            logger.info('config.yml found and parsed. Hopefully you have populated it in a valid manner.')
    except FileNotFoundError:
        logger.exception('config.yml not found. Clone and edit sample.config.yml to setup required parameters.')
        return
    # get time entries from toggl
    toggl = Toggl(config['toggl']['api_key'], config['email'])
    time_entries = toggl.get_all_time_entries(since, until)
    # setup transformer
    Transformer = get_class(transformer_class)
    transformer = Transformer(
        config['administrative_tickets'],
        config['default_time_entry_description']
    )
    # pipe them to Redmine
    redmine = Redmine(config['redmine']['url'], config['redmine']['api_key'])
    for time_entry in time_entries:
        try:
            transformed = transformer.transform(time_entry)
            if not transformed['issue_id']:
                logger.warn('Could not determine issue ID. Skipping {}h on {} for: {}'.format(
                    transformed['hours'],
                    transformed['spent_on'],
                    transformed['comments']
                ))
                continue
            redmine.create_time_entry(
                transformed['issue_id'],
                transformed['spent_on'],
                transformed['hours'],
                config['redmine']['activity_id'],
                transformed['comments']
            )
        except:
            logger.exception('Error occurred while processing')


if __name__ == '__main__':
    # parse arguments from CLI
    today = date.today()
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config-file',
        default='config.yml',
        type=str,
        help='Path to the configuration file. Defaults to config.yml. Clone the sample provided to create a new configuration file.'
    )
    parser.add_argument(
        '--since',
        default=today.isoformat(),
        type=date_from_iso_format,
        help='ISO 8601 date (YYYY-MM-DD) format. Defaults to today.'
    )
    parser.add_argument(
        '--until',
        default=today.isoformat(),
        type=date_from_iso_format,
        help='ISO 8601 date (YYYY-MM-DD) format. Defaults to today.'
    )
    parser.add_argument(
        '--log-level',
        default='INFO',
        type=str,
        help='Set the logging level for the application. Uses standard python logging levels. Default to INFO'
    )
    parser.add_argument(
        '--transformer-class',
        default='transformers.SimpleTransformer',
        type=str,
        help='Specify the transfomer used to create the issue ID and comments from the toggl time entry. Defaults to transformers.SimpleTransformer'
    )
    args = parser.parse_args()
    # configure logging
    logging.basicConfig(level=args.log_level)
    logger = logging.getLogger('toggl2redmine')
    # setup uncaught exception handler
    sys.excepthook = handle_uncaught_exceptions
    logger.info('Parsed argument --config: {}'.format(args.config_file))
    logger.info('Parsed argument --since: {}'.format(args.since))
    logger.info('Parsed argument --until: {}'.format(args.until))
    # all good now, run the main method
    main(args.config_file, args.since, args.until, args.transformer_class)
