from abc import ABC, abstractmethod


def validate_dict(class_name, keys, d):
    missing_keys = []
    for key in keys:
        if key not in d:
            missing_keys.append(key)
    if len(missing_keys) > 0:
        raise AssertionError('{} does not return the following keys: {}'.format(
            class_name,
            ', '.join(missing_keys)
        ))

class BaseTransformer(ABC):
    _administrative_tickets = None
    _default_time_entry_description = None

    def __init__(self, administrative_tickets=None, default_time_entry_description=None):
        if not administrative_tickets:
            self._administrative_tickets = {}
        else:
            self._administrative_tickets = administrative_tickets
        self._default_time_entry_description = default_time_entry_description
    
    @abstractmethod
    def _transform_time_entry(self, toggl_time_entry):
        """Implemented by derived transformer."""

    def transform(self, toggl_time_entry):
        """
        Accepts a toggl time entry dict.
        Should return a Redmine ticket number and description.
        """
        result = self._transform_time_entry(toggl_time_entry)
        keys = ['issue_id', 'spent_on', 'hours', 'comments']
        validate_dict(self.__class__.__name__, keys, result)
        return result
