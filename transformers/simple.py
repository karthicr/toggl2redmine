import dateparser

from decimal import Decimal, getcontext

try:
    from .base import BaseTransformer
except:
    from base import BaseTransformer

class SimpleTransformer(BaseTransformer):
    def __init__(self, administrative_tickets=None, default_time_entry_description=None):
        super().__init__(administrative_tickets, default_time_entry_description)
        getcontext().prec=2

    def _get_administrative_ticket(self, start):
        return self._administrative_tickets.get(start.strftime('%Y-%m'), None)

    def _transform_time_entry(self, toggl_time_entry):
        """
        The toggl time entry description is split into tokens
        The first token, if digits, is assumed to be the Redmine issue id.
        If the first token is not digits, then an administrative ticket
        for the month is determined from the config and the issue id for
        that ticket is returned.
        If the issue id cannot be determined in a reliable manner,
        None is returned. The Redmine client will skip such time entries.
        """
        result = {}
        start = dateparser.parse(toggl_time_entry['start'])
        tokens = toggl_time_entry['description'].split()
        if len(tokens) > 1 and tokens[0].isdigit():
            result['issue_id'] = tokens[0]
            if tokens[1] in ['-', ':']:
                result['comments'] = ' '.join(tokens[2:])
            else:
                result['comments'] = ' '.join(tokens[1:])
        elif len(tokens) == 1 and tokens[0].isdigit():
            result['issue_id'] = tokens[0]
            result['comments'] = self._default_time_entry_description
        else:
            result['issue_id'] = self._get_administrative_ticket(start)
            result['comments'] = toggl_time_entry['description']
        result['spent_on'] = start.strftime('%Y-%m-%d')
        result['hours'] = str(Decimal(toggl_time_entry['dur'] / Decimal(1000*60*60)))
        return result
