import json
import logging
import math
import requests
import time


# configure logging
logger = logging.getLogger(__name__)


class Toggl(object):
    # https://github.com/toggl/toggl_api_docs
    _api_url_user = 'https://www.toggl.com/api/v8/me'
    _api_url_reports = 'https://toggl.com/reports/api/v2/details'
    _api_key = None
    _user_agent = None
    _user_data = None
    _s = None
    _json_dump_indent = 2

    def __init__(self, api_key, email):
        self._api_key = api_key
        self._user_agent = 'toggl2redmine: {}'.format(email)
        self._s = requests.Session()
        self._s.auth = (api_key, 'api_token')
        self._user_data = self.get_user_data()

    def get_user_data(self):
        # https://github.com/toggl/toggl_api_docs/blob/master/toggl_api.md
        # https://github.com/toggl/toggl_api_docs/blob/master/chapters/users.md
        logger.info('Fetching current user data')
        r = self._s.get(self._api_url_user)
        r.raise_for_status()
        result = r.json()
        logger.debug('\n{}'.format(json.dumps(result, indent=self._json_dump_indent)))
        return result['data']
    
    def get_workspace_time_entries(self, since, until, workspace, page):
        # https://github.com/toggl/toggl_api_docs/blob/master/reports.md
        # https://github.com/toggl/toggl_api_docs/blob/master/reports/detailed.md
        logger.info('Fetching time entries for {} since {} until {}.'.format(
            workspace['name'],
            since,
            until
        ))
        params = {
            'workspace_id': workspace['id'],
            'since': since,
            'until': until,
            'user_agent': self._user_agent,
            'page': page
        }
        r = self._s.get(self._api_url_reports, params=params)
        r.raise_for_status()
        result = r.json()
        logger.info('Fetched {} time entries for {}'.format(len(result['data']), workspace['name']))
        logger.debug('\n{}'.format(json.dumps(result, indent=self._json_dump_indent)))
        return result
    
    def get_all_time_entries(self, since, until):
        logger.info('Fetching time entries for all workspaces since {} until {}'.format(since, until))
        result = []
        for workspace in self._user_data['workspaces']:
            current_page = 1
            total_pages = None
            while True:
                response = self.get_workspace_time_entries(since, until, workspace, current_page)
                result.extend(response['data'])
                if not total_pages:
                    total_pages = math.ceil(response['total_count'] / response['per_page'])
                if current_page < total_pages:
                    current_page += 1
                    time.sleep(1)
                    logger.info('Sleeping 1 second to honor toggl API rate limit.')
                else:
                    break
        logger.info('Fetched {} time entries for all workspaces'.format(len(result)))
        return result


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    toggl = Toggl('6e32569e6f6d533fde2ac63a0195bbc1', 'karthicr@gmail.com')
    toggl.get_all_time_entries('2018-11-19', '2018-11-19')
