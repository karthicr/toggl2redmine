# toggl2redmine

This script will read the time entries from toggl via the REST API and pipe that to Redmine.

## Requirements

- Python 3+ (built on 3.4.3)
- Install requirements from `requirements.txt`

## Configuration

You will need the following:

- toggl API key
- toggl workspace IDs
- Redmine URL
- Redmine API key
- Redmine activity ID

Clone the sample config YAML file and populate the values:

```
cp sample.config.yml to config.yml
```

## Input

- Time entry from toggl API will be split into token on whitespace.
- The first token will be used as the Redmine ticket number against which the time should be logged.
  - If the first token is not all digits, a default ticket defined in the `config.yml` for administrative tasks is used.
  - The administrative ticket is defined by the year and month.
- The rest of the tokens will be used as description for the time entry.
  - If no description is found, a default value defined in `config.yml` is used.

## Usage

```
$ ./toggl2redmine.py --help
usage: toggl2redmine.py [-h] [--since SINCE] [--until UNTIL]

optional arguments:
  -h, --help     show this help message and exit
  --since SINCE  ISO 8601 date (YYYY-MM-DD) format. Defaults to today.
  --until UNTIL  ISO 8601 date (YYYY-MM-DD) format. Defaults to today.

```

**Note:** Per [toggl's report API](https://github.com/toggl/toggl_api_docs/blob/master/reports.md#request-parameters "toggl's report API"), maximum date span (until - since) is one year.
