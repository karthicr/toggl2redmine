import json
import logging
import requests


# configure logging
logger = logging.getLogger(__name__)


class Redmine(object):
    _api_url = None
    _api_key = None
    _s = None

    def __init__(self, api_url, api_key):
        self._api_url = api_url
        self._api_key = api_key
        self._s = requests.Session()
        self._s.auth = (self._api_key, 'api_token')
        self._s.headers.update({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        })

    def _get_resource_url(self, resource):
        if self._api_url[-1] == '/':
            result = '{}{}.json'
        else:
            result = '{}/{}.json'
        return result.format(self._api_url, resource)
    
    def create_time_entry(self, issue_id, spent_on, hours, activity_id, comments):
        # http://www.redmine.org/projects/redmine/wiki/Rest_api
        # http://www.redmine.org/projects/redmine/wiki/Rest_TimeEntries#Creating-a-time-entry
        logger.info('Adding {}h on {} to #{} for: {}'.format(hours, spent_on, issue_id, comments))
        payload = {
            'time_entry': {
                'issue_id': issue_id,
                'spent_on': spent_on,
                'hours': hours,
                'activity_id': activity_id,
                'comments': comments
            }
        }
        r = self._s.post(self._get_resource_url('time_entries'), json=payload)
        r.raise_for_status()
