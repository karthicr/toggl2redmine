from importlib import import_module


def get_class(fully_qualified_class_name):
    tokens = fully_qualified_class_name.rsplit('.', 1)
    if len(tokens) == 1:
        raise ValueError('{} doesn\'t appear to be a fully qualified class name.'.format(fully_qualified_class_name))
    return getattr(import_module(tokens[0]), tokens[1])
